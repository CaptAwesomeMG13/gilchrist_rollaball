﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
/*
 * Script for player controller
 */
public class PlayerController : MonoBehaviour
{
    //Variables you can change in Unity
    public float speed;
    public Text countText;
    public Text winText;

    //new added features
    public GameObject[] destroyWall;
    public GameObject particles;

    private int count;
    private Rigidbody rb;
    
    void Start()
    {
        rb = GetComponent<Rigidbody>();
        count = 0;
        SetCountText ();
        winText.text = "";
        particles.SetActive(false);//autoset to off
    }
    
    //turns on particles
    void Update()
    {
        if (count == 60)
        {
            particles.SetActive(true);
        }
    }

    //Speed Code
    private void FixedUpdate()
    {
        float moveHorizontal = Input.GetAxis("Horizontal");
        float moveVertical = Input.GetAxis("Vertical");

        Vector3 movement = new Vector3(moveHorizontal, 0.0f, moveVertical);

        rb.AddForce(movement * speed);
    }

    //Collision Code
    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag ( "Pick Up"))
        {
            other.gameObject.SetActive (false);
            count = count + 1;
            SetCountText ();
        }
        
        if (count == 12)//first wall
        {
            Destroy(destroyWall[0]);
        }
        if (count == 24)//second wall
        {
            Destroy(destroyWall[1]);
        }
        if (count == 36)//third wall
        {
            Destroy(destroyWall[2]);
        }
        if (count == 48)//fourth wall
        {
            Destroy(destroyWall[3]);
        }
    }

    //Count Code
    void SetCountText ()
    {
        countText.text = "Count: " + count.ToString ();
        if (count >= 60)
        {
            winText.text = "You Win!";
        }
    }
}
